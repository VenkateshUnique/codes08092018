package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MergeLeadsPage extends ProjectMethods
{
	public MergeLeadsPage clickFromLeadimage()
	{
		WebElement clickfirstimage = locateElement("xpath", "(//img[@alt='Lookup'])[1]");
		click(clickfirstimage);
		return this;
	}
	

}
